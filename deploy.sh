#!/usr/bin/env bash
set -e

WWW_HOST="[2001:41d0:2:a420:138::2]"
WWW_PATH="/var/www/default/"

read -p "About to deploy. Are you sure? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
  echo "Aborting"
  exit 1
fi

set -x
rsync --perms --chmod=a+r -r -v public/ $WWW_HOST:$WWW_PATH
